# surgewaves
## Meridian.id's Project Management Framework.

> Surgewaves consist of word 'Surge' and 'Waves' that mean literally as 'Lonjakan' and 'Gelombang' in Bahasa. So it have a hope that this Framework will surging the project flow.

## Structures

Surgewaves Framework will consist of
1. Project Flows
2. Project Roles
3. Frameworks Components

Framework folder structures will created mostly according Project Flow and every Project Flow will be enhanced by Frameworks Components.

## Project Flow



## Project Roles

A standard projects in Meridian.id will have team consist of these roles
1. Project Manager
2. Sales
3. Analyst
4. UI/UX Designer
5. Developer
    * Backend Developer
    * Frontend Developer
    * Full-stack Developer
    * Mobile Developer
6. Quality Assurance and Tester

An Ignite projects in Meridian.id will have team consist of these roles
1. Project Manager
2. Sales
3. Developer
    * Full-stack Developer
    * (TBD)

## Framework Components
Surgewaves Framework will be composed by four major components,
1. General Body of Knowledge
2. Standard, Operation, and Procedure
3. Checklist or Compliance Assesment
4. Artifacts