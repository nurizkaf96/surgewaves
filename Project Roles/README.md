# Understanding Roles
Before started jumped to your project. Please understand who you are in the project. In Meridian.id sometimes you have more than one roles so please remind to fulfill all the roles given to you.

## Project Manager

> Project Manager is someone who
> {keterangan tentang si project managernya}

General Responsibility:
1. Planning Work Breakdown Structures
2. 
3. 

## Sales and Marketing

### Sales

> Sales is someone who
> {keterangan tentang si sales}

General Responsibility:
1. 
2. 

### Marketing

> Marketing is someone who
> {keterangan tentang si marketing}

General Responsibility:
1. 
2. 

## Analysis & Design

### Analyst

> Analyst is someone who
> {keterangan tentang si analis}

General Responsibility:
1. 
2. 

### UX Designer

> UX Designer is someone who
> {keterangan tentang si uxd}

General Responsibility:
1. 
2. 

### UI Designer

> UI Designer is someone who
> {keterangan tentang si uid}

General Responsibility:
1. 
2. 

## Development

### Full Stack Developer

> Full Stack Developer is someone who
> {keterangan tentang si fullstack}

General Responsibility:
1. 
2. 

### Front End Developer

> Front End Developer is someone who
> {keterangan tentang si FE}

General Responsibility:
1. 
2. 

### Back End Developer

> Back End Developer is someone who
> {keterangan tentang si BE}

General Responsibility:
1. 
2. 

### Mobile Developer

> Mobile Developer is someone who
> {keterangan tentang si androdev}

General Responsibility:
1. 
2. 